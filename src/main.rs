use std::{fs::File, io::{Result, Read}};

// PART 1
#[cfg(not(feature="part2"))]
fn main() -> Result<()> {
    Ok({println!("output = {}", run(12, 2)?)})
}

fn run(noun: usize, verb: usize) -> Result<usize> {
    let (mut file, mut buffer, mut ip) = (File::open("input")?, String::new(), 0);
    file.read_to_string(&mut buffer)?;
    let mut buffer: Vec<usize> = buffer.replace('\n', "").replace(' ', "").split_terminator(',').map(|x| x.parse().unwrap()).collect();
    buffer[1] = noun; buffer[2] = verb;
    while ip + 3 < buffer.len() {
        let ins = [buffer[ip], buffer[ip + 1], buffer[ip + 2], buffer[ip + 3]];
        match ins[0] {
            99 => return Ok(buffer[0]),
            1 => buffer[ins[3]] = buffer[ins[1]] + buffer[ins[2]],
            2 => buffer[ins[3]] = buffer[ins[1]] * buffer[ins[2]],
            _ => return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Invalid Intcode"))
        }

        ip += 4;
    }
    Ok(buffer[0])
}

// PART 2
#[cfg(feature="part2")]
fn main() {
    let desired: usize = std::env::args().skip(1).next().expect("Pass the expected output").parse().unwrap();
    for noun in 0..100 {
        for verb in 0..100 {
            if let Ok(output) = run(noun, verb){
                if output == desired { println!("{} Works", 100 * noun + verb) }
            }
        }
    }
}

#[test]
fn compare_examples() {
    let run = |buffer: &mut [usize]| {
        let mut buffer = Vec::from(&buffer[..]);
        let mut ip = 0;
        while ip + 3 < buffer.len() {
            let ins = [buffer[ip], buffer[ip + 1], buffer[ip + 2], buffer[ip + 3]];
            match ins[0] {
                99 => return Ok(buffer),
                1 => buffer[ins[3]] = buffer[ins[1]] + buffer[ins[2]],
                2 => buffer[ins[3]] = buffer[ins[1]] * buffer[ins[2]],
                _ => return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Invalid Intcode"))
            }
    
            ip += 4;
        }
        Ok(buffer)
    };

    assert_eq!(run(&mut [1,9,10,3,2,3,11,0,99,30,40,50]).unwrap(), vec![3500,9,10,70,2,3,11,0,99,30,40,50]);
    assert_eq!(run(&mut [1,0,0,0,99]).unwrap(), vec![2,0,0,0,99]);
    assert_eq!(run(&mut [2,3,0,3,99]).unwrap(), vec![2,3,0,6,99]);
    assert_eq!(run(&mut [2,4,4,5,99,0]).unwrap(), vec![2,4,4,5,99,9801]);
    assert_eq!(run(&mut [1,1,1,4,99,5,6,0,99]).unwrap(), vec![30,1,1,4,2,5,6,0,99]);
    
}