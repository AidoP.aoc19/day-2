# Advent of Code Day 2

Solutions for part 1 and 2 of AoC 2019.

Part 2 is compiled by default.
`cargo run --no-default-features` for part 1
`cargo run -- <expected_output>` for part 2